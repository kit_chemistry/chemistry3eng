//JQUERY SELECTORS
var derButton, factsButton, labButton, links,
	menuFrame, frames, model, modelContent;

//SOUNDS
var audio = [],
	sprite = [],
	typingText = [];

//TIMEOUTS
var timeout = [],
	launch = [],
	globalName;

//OTHER GLOBAL DATA
var shots, currentShot;

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.ended = function(callBack)
{
	this.audio.addEventListener("ended", callBack);
};

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum),
			preLoadImage = $("#pre-load .pre-load-image");
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			preLoadImage.css("width", loadPercentage + "%");
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

var createDragTask = function(prefix, draggables, finishFunction){
	var draggabillies = [],
		vegetable, basket;
	
	for (var i = 0; i < draggables.length; i ++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target); 
		vegetable.css("background-color", "#5AA0F5");
	}
	
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			vegetable.remove();
			basket.css("background-color", vegetable.css("background-color"));
			basket.html(vegetable.html());

			if(!$(prefix + ".ball").length)
			{
				finishFunction();
			}
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
			vegetable.css("background-color", "");
			vegetable.fadeIn(0);
		}
	}
	
	for (var i = 0; i < draggabillies.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
}

var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var setMenuStuff = function(){
	var enterButton = $(".enter-button"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button"),
		password = $(".password"),
		error = $(".error"),
		name = $(".name");
		name.val("");
		password.val("");
		
		
		sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");
	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}

var matches = function(ch, exp){
	for(var i = 0; i < exp.length; i++)
		if(exp[i] === ch)
			return true;
	
	return false;
}

launch["frame-000"] = function(){
	
};

launch["frame-101"] = function(){
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var prefix = "#" + theFrame.attr("id") + " ",
		guysPlaying = $(prefix + ".guys-playing"), 
		guysPlayingAnimated = $(prefix + ".guys-playing-animated"), 
		clock = $(prefix + ".clock"),
		firework = $(prefix + ".firework"),
		cloud = $(prefix + ".cloud"),
		newPlace = $(prefix + " .new-place");
		
	
	sprite[0] = new Motio(clock[0], {
		fps: 1/5,
		frames: 3
	});
	
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			audio[2].play();
			firework.fadeOut(0);
			guysPlaying.fadeIn();
			guysPlayingAnimated.fadeOut(0);
			timeout[1] = setTimeout(function(){
				cloud.fadeIn(3000);
			}, 1000);
			timeout[2] = setTimeout(function(){
				newPlace.addClass("transition-3s");
				timeout[3] = setTimeout(function(){
					newPlace.css("opacity", "1");
					audio[3].play();
					timeout[4] = setTimeout(function(){
						theFrame.attr("data-done", "true");
						hideEverythingBut($("#frame-102"));
						sendCompletedStatement(1);
					}, 4000);
				}, 2000); 
			}, 4000);
		}
	});
	
	audio[0] = new Audio("audio/firework.mp3");
	audio[1] = new Audio("audio/clock-tick.mp3");
	audio[2] = new Audio("audio/chimes.mp3");
	audio[3] = new Audio("audio/magic.mp3");
	
	audio[0].addEventListener("ended", function(){
		firework.fadeOut(0);
	});
	
	guysPlayingAnimated.fadeOut(0);
	firework.fadeOut(0);
	cloud.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	newPlace.css("opacity", "0");
	
	startButtonListener = function(){
		audio[0].play();
		audio[1].play();
		firework.fadeIn(0);
		sprite[0].play();
		guysPlaying.fadeOut(0);
		guysPlayingAnimated.fadeIn(0);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-102"] = function(){
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boysBack = $(prefix + ".boys-back"),
		boysWalking = $(prefix + ".boys-walking"),
		bg = $(prefix + ".bg-base");
	
	sprite[0] = new Motio(boysWalking[0], {
		fps: 3,
		frames: 7
	});
	
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			sprite[0].pause();
	});
	
	boysWalking.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/footsteps.mp3");
	audio[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(2);
	});
	
	startButtonListener = function(){
		bg.addClass("bg-base-2");
		timeout[1] = setTimeout(function(){
			boysBack.fadeOut(0);
			bg.fadeOut(0);
			boysWalking.fadeIn(0);
			theFrame.css("background-image", "url('pics/debereiner.png')");
			sprite[0].play();
			audio[0].play();
		}, 4000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-103"] = function(){
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		boysBack = $(prefix + ".boys-back"),
		debereinerWriting = $(prefix + ".debereiner-writing"),
		debereinerSpeaking = $(prefix + ".debereiner-speaking"),
		debereinerIdle = $(prefix + ".debereiner-idle");
	
	audio[0] = new Audio("audio/s5-1.mp3");
	audio[1] = new Audio("audio/s6-1.mp3");
		
	audio[0].addEventListener("ended", function(){
		debereinerWriting.fadeOut(0);
		debereinerSpeaking.fadeIn(0);
		audio[1].play();
	});
	
	audio[1].addEventListener("ended", function(){
		debereinerSpeaking.fadeOut(0);
		debereinerIdle.fadeIn(0);
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(3);
		}, 3000);
	});
	
	boysBack.css("top", "100%");
	debereinerSpeaking.fadeOut(0);
	debereinerIdle.fadeOut(0);
	debereinerWriting.fadeIn(0);
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		boysBack.animate({
			"top": "0%"
		}, 1000, function(){
			audio[0].play();
		});
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-104"] = function(){
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyAsking = $(prefix + ".boy-asking"),
		boysIdle = $(prefix + ".boys-idle"),
		debereinerIdle = $(prefix + ".debereiner-idle"),
		debereinerSpeaking = $(prefix + ".debereiner-speaking"),
		year1829 = $(prefix + ".year-1829");
	
	boyAsking.fadeOut(0);
	debereinerSpeaking.fadeOut(0);
	year1829.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s7-1.mp3");
	audio[1] = new Audio("audio/s8-1.mp3");
	audio[2] = new Audio("audio/s9-1.mp3");
	audio[3] = new Audio("audio/s10-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		boysIdle.fadeIn(0);
		boyAsking.fadeOut(0);
		timeout[1] = setTimeout(function(){
			audio[1].play();
			debereinerIdle.fadeOut(0);
			debereinerSpeaking.fadeIn(0);
			year1829.fadeIn("slow");
			year1829.animate({
				"left": "-150%"
			},5000, function(){
				year1829.fadeOut("slow");
			});
		}, 1000);
	});
	audio[1].addEventListener("ended", function(){
		debereinerIdle.fadeIn(0);
		debereinerSpeaking.fadeOut(0);
		timeout[2] = setTimeout(function(){
			boysIdle.fadeOut(0);
			boyAsking.fadeIn(0);
			audio[2].play();
		}, 1000);
	});
	
	audio[2].addEventListener("ended", function(){
		boysIdle.fadeIn(0);
		boyAsking.fadeOut(0);
		timeout[3] = setTimeout(function(){
			audio[3].play();
			debereinerIdle.fadeOut(0);
			debereinerSpeaking.fadeIn(0);
		}, 1000);
	});
	
	audio[3].addEventListener("ended", function(){
		debereinerIdle.fadeIn(0);
		debereinerSpeaking.fadeOut(0);
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(4);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boyAsking.fadeIn(0);
		boysIdle.fadeOut(0);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
}

launch["frame-105"] = function(){
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		fields = $(prefix + ".field"),
		checkButton = $(prefix + ".check-button");
	
	audio[0] = new Audio("audio/s11-1.mp3");
	
	checkButton.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		audio[0].play();
	};
	
	var areDirty = function()
	{
		for (var i = 0; i < fields.length; i++)
			if($(fields[i]).val() === "")
				return false;
			
		return true;
	}
	
	var fieldListener = function(){
		var currField = $(this);
		var exp = ["0","1","2","3","4","5","6","7","8","9", ".", ","];
		var currValue = currField.val();
		
		if(matches(currValue.charAt(currValue.length - 1), exp))
		{
			$(prefix + "." + currField.attr("data-key")).html(currValue);
		}
		else
		{
			var currValueArray = currValue.split("");
			currValueArray.pop();
			currValue = currValueArray.join("");
			currField.val(currValue);
		}
		if(areDirty())
			checkButton.fadeIn(300);
	}
	
	fields.off("keyup", fieldListener);
	fields.on("keyup", fieldListener);
	for(var i = 0; i < fields.length; i++)
		$(fields[i]).val("");
	
	var checkButtonListener = function(){
		correctAnswers = ["23.0195","88.705","76.3619"];
		for(var i = 0; i < fields.length; i++)
		{
			var fvalue = fields[i].value;
			if(fvalue === correctAnswers[i])
				$(fields[i]).addClass("green");
			else
				$(fields[i]).addClass("red");
		}
			
		timeout[1] = setTimeout(function(){
			for(var i = 0; i < fields.length; i++)
			{
				$(fields[i]).addClass("green");
				$(fields[i]).val(correctAnswers[i]);
			}
		}, 4000);
	}
	
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var draggables = $(prefix + ".ball");
	var draggabillies = [],
		vegetable, basket;
	
	for (var i = 0; i < draggables.length; i ++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
	}
	
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			vegetable.remove();
			var bgImage = "url('pics/" + vegetable.attr("data-bg") + "-green.png')";
			basket.css("background-image", bgImage);
			if(!$(prefix + ".ball").length)
			{
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(5);
			}
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
			vegetable.fadeIn(0);
		}
	}
	
	for (var i = 0; i < draggabillies.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
}

launch["frame-106"] = function(){
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth");
	
	audio[0] = new Audio("audio/s12-1.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	boyMouth.fadeOut(0);
	
	audio[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(6);
		boyMouth.fadeOut(0);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boyMouth.fadeIn(0);
	}
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"] = function(){
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boysWalking = $(prefix + ".boys-walking-left");
	
	audio[0] = new Audio("audio/footsteps.mp3");
		
	sprite[0] = new Motio(boysWalking[0], {
		fps: 3,
		frames: 7
	});
	
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(7);
		}
	});
	
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		audio[0].play();
		sprite[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-108"] = function(){
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boysIdle = $(prefix + ".boys-idle"),
		boysSpeaking = $(prefix + ".boys-speaking"),
		reinaIdle = $(prefix + ".reina-idle"), 
		reinaSpeaking = $(prefix + ".reina-speaking"),
		year1865 = $(prefix + ".year-1865"),
		tableTask = $(prefix + ".table-task"),
		piano = $(prefix + ".piano"),
		table2 = $(prefix + ".table-2"),
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket"),
		checkButton = $(prefix + ".check-button"),
		heroes = $(prefix + ".hero");
	
	sprite[0] = new Motio(reinaIdle[0], {
		fps: 1, 
		frames: 2
	});
	
	sprite[1] = new Motio(piano[0], {
		fps: 2,
		frames: 14
	});
	
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			timeout[4] = setTimeout(function(){
				piano.fadeOut(0);
			}, 2000);
		}
	});
	
	audio[0] = new Audio("audio/s15-1.mp3");
	audio[1] = new Audio("audio/s16-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		reinaIdle.fadeIn(0);
		reinaSpeaking.fadeOut(0);
		timeout[5] = setTimeout(function(){
			tableTask.fadeIn(500);
			balls.fadeIn(500);
			baskets.fadeIn(500);
			heroes.fadeOut(500);
		}, 2000);
	});
		
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		basket.attr("data-color", "#009900");
		vegetable.remove();
	};
	
	var failFunction = function(vegetable, basket){
		if(basket.hasClass("basket"))
		{
			basket.html(vegetable.html());
			basket.attr("data-color", "#EA361A");
			vegetable.remove();
		}
	};
	
	var finishCondition = function(){
		return $(prefix + ".ball").length <= 1;
	};
	
	var finishFunction = function(){
		checkButton.fadeIn(0);
	};
	
	var dragTask = DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	
	fadeNavsOut();
	fadeLauncherIn();
	boysSpeaking.fadeOut(0);
	reinaSpeaking.fadeOut(0);
	year1865.fadeOut(0);
	tableTask.fadeOut(0);
	piano.fadeOut(0);
	table2.fadeOut(0);
	checkButton.fadeOut(0);
	balls.fadeOut(0);
	baskets.fadeOut(0);
	
	var checkButtonListener = function(){
		for(var i = 0; i < baskets.length; i++)
			$(baskets[i]).css("background-color", $(baskets[i]).attr("data-color"));
		
		timeout[5] = setTimeout(function(){
			for(var i = 0; i < baskets.length; i++)
			{
				currBasket = $(baskets[i]);
				currBasket.css("background-color", "#009900");
				currBasket.html(currBasket.attr("data-correct"));
			}
		}, 3000);
		
		timeout[5] = setTimeout(function(){
			tableTask.fadeOut(0);
			heroes.fadeIn(0);
			baskets.fadeOut(0);
			balls.fadeOut(0);
			checkButton.fadeOut(0);
			reinaIdle.fadeOut(0);
			reinaSpeaking.fadeIn(0);
			audio[1].play();
			timeout[6] = setTimeout(function()
			{
				reinaSpeaking.fadeOut(0);
				heroes.fadeOut(0);
				piano.fadeIn(0);
				sprite[1].play();
			}, 23000);
			timeout[7] = setTimeout(function()
			{
				table2.fadeIn(500);
			}, 33000);
			timeout[8] = setTimeout(function()
			{
				audio[1].pause();
				timeout[9] = setTimeout(function(){
					theFrame.attr("data-done", "true");
					fadeNavsInAuto();
					sendCompletedStatement(8);
				}, 1000);
			}, 35000);			
		}, 8000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			sprite[0].to(1, true);
		}, 1000);
		timeout[2] = setTimeout(function(){
			audio[0].play();
			reinaSpeaking.fadeIn(0);
		}, 3000);
		timeout[3] = setTimeout(function(){
			year1865.fadeIn(500);
			year1865.animate({
				"left": "150%"
			}, 5000, function(){
				year1865.fadeOut(0);
			});
		}, 8000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-109"] = function(){
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		manMouth = $(prefix + ".man-mouth"),
		boyMouth = $(prefix + ".boy-mouth");
	
	audio[0] = new AudioPiece("s16-1", 35, 99);
	audio[1] = new Audio("audio/s17-1.mp3");
		
	audio[0].ended(function(){
		manMouth.fadeOut(0);
		timeout[1] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audio[1].play();
		}, 1000);
	});
	
	audio[1].addEventListener("ended", function(){
		boyMouth.fadeOut(0);
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(9);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	manMouth.fadeOut(0);
	boyMouth.fadeOut(0);
		
	startButtonListener = function(){
		manMouth.fadeIn(0);
		audio[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(9);
	}, 2000);
}

launch["frame-110"] = function(){
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg-base");
	
	fadeNavsOut();
	fadeLauncherIn();
	bg.removeClass("bg-base-2");
		
	startButtonListener = function(){
		bg.addClass("bg-base-2");
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-111"));
			sendCompletedStatement(10);
		}, 3000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(10);
	}, 2000);
}

launch["frame-111"] = function(){
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boysWalking = $(prefix + ".boys-walking-left");
	
	sprite[0] = new Motio(boysWalking[0], {
		fps: 4,
		frames: 7
	});
	
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	audio[0] = new Audio("audio/footsteps.mp3");
		
	audio[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(11);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		audio[0].play();
		sprite[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(11);
	}, 2000);
}

launch["frame-112"] = function(){
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		mendeleevTableShadow = $(prefix + ".mendeleev-table-shadow"), 
		mendeleevIdle = $(prefix + ".mendeleev-idle"), 
		mendeleevSmiling = $(prefix + ".mendeleev-smiling"), 
		mendeleevSpeaking = $(prefix + ".mendeleev-speaking"), 
		crowd = $(prefix + ".crowd"), 
		boysBack = $(prefix + ".boys-back"),
		boysRaiseHand = $(prefix + ".boys-raise-hand");
	
	sprite[0] = new Motio(crowd[0], {
		fps: 3, 
		frames: 5
	});
	
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			audio[0].play();
			mendeleevIdle.fadeOut(0);
			mendeleevSmiling.fadeIn(0);
			timeout[1] = setTimeout(function(){
				sprite[0].to(0, true);
				mendeleevSmiling.fadeOut(0);
				mendeleevSpeaking.fadeIn(0);
				mendeleevTableShadow.fadeIn(0);
			}, 4000);
			timeout[2] = setTimeout(function(){
				mendeleevSmiling.fadeIn(0);
				mendeleevSpeaking.fadeOut(0);
				boysRaiseHand.fadeIn(0);
				boysBack.fadeOut(0);
			}, 29000);
			timeout[3] = setTimeout(function(){
				audio[0].pause();
			}, 46200);
			timeout[4] = setTimeout(function(){
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(12);
			}, 48000);
		}
	});
	
	mendeleevSmiling.fadeOut(0);
	mendeleevSpeaking.fadeOut(0);
	boysRaiseHand.fadeOut(0);
	mendeleevTableShadow.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s20-1.mp3");
			
	startButtonListener = function(){
		sprite[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(12);
	}, 2000);
}

launch["frame-113"] = function(){
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		manMouth = $(prefix + ".man-mouth"),
		boyMouth = $(prefix + ".boy-mouth"),
		preTablet = $(prefix + ".pre-tablet"),
		tabletShowing = $(prefix + ".tablet-showing"),
		tablet = $(prefix + ".tablet"),
		tabletHand = $(prefix + ".tablet-hand"),
		tabletHighlight = $(prefix + ".tablet-highlight"),
		castle = $(prefix + ".castle"),
		castleElements = $(prefix + ".castle-elements"),
		castleMetals = $(prefix + ".castle-metals"),
		castleNonMetals = $(prefix + ".castle-non-metals"),
		castleGroup1 = $(prefix + ".castle-group-1"),
		castleBoys = $(prefix + ".castle-boys");
	
	audio[0] = new Audio("audio/s20-1.mp3");
	audio[1] = new Audio("audio/s21-1.mp3");
		
	audio[0].addEventListener("ended", function(){
		audio[1].play();
		preTablet.fadeIn(0);
		manMouth.fadeIn(0);
		tablet.fadeOut(0);
		tabletHand.fadeOut(0);
		tabletHighlight.fadeOut(0);
		tabletShowing.fadeOut(0);
		
		timeout[3] = setTimeout(function(){
			boyMouth.fadeIn(0);
			manMouth.fadeOut(0);
		}, 2000);
		timeout[4] = setTimeout(function(){
			boyMouth.fadeOut(0);
			manMouth.fadeIn(0);
			castle.fadeIn(0);
			castleBoys.fadeIn(1000);
			castleElements.fadeIn(1000);
		}, 16000);
		timeout[5] = setTimeout(function()
		{
			castleMetals.fadeIn(0);
		}, 32000);
		timeout[6] = setTimeout(function()
		{
			castleMetals.fadeOut(0);
			castleNonMetals.fadeIn(0);
		}, 34000);
		timeout[7] = setTimeout(function()
		{
			castleNonMetals.fadeOut(0);
			castleGroup1.fadeIn(0);
		}, 37000);
		timeout[8] = setTimeout(function()
		{
			castleGroup1.fadeOut(0);
			timeout[9] = setTimeout(function(){
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(13);
			}, 8000);
		}, 40000);
	});
	
	tabletShowing.fadeOut(0);
	tablet.fadeOut(0);
	tabletHand.fadeOut(0);	
	tabletHighlight.fadeOut(0);
	castle.fadeOut(0);
	castleElements.fadeOut(0);
	castleMetals.fadeOut(0);
	castleNonMetals.fadeOut(0);
	castleGroup1.fadeOut(0);
	castleBoys.fadeOut(0);
	boyMouth.fadeOut(0);
	manMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		preTablet.fadeOut(0);
		audio[0].currentTime = 46.2;
		audio[0].play();
		tabletShowing.fadeIn(0);
		timeout[1] = setTimeout(function(){
			tablet.fadeIn(0);
			tabletHand.fadeIn(0);
		}, 1000);
		timeout[2] = setTimeout(function(){
			tabletHighlight.fadeIn(0);
		}, 6000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-113-a"] = function(){
		theFrame = $("#frame-113-a"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		tasks = $(prefix + ".task"),
		activeTask = 1,
		balls = $(prefix + ".ball"),
		checkButtons = $(prefix + ".check-button");
	
	fadeNavsOut();
	fadeLauncherIn();
	tasks.fadeOut(0);
	checkButtons.fadeOut(0);
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket){
		if(basket.html() === "")
		{
			basket.html(vegetable.html());
			basket.attr("data-color", "green");
			basket.css("width", vegetable.html().length + "em");
			vegetable.remove();
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
		}
	};
	
	var failFunction = function(vegetable, basket){
		if(basket.html() === "" && basket.hasClass("basket"))
		{
			basket.html(vegetable.html());
			basket.attr("data-color", "red");
			basket.css("width", vegetable.html().length + "em");
			vegetable.remove();
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
		}
	};
	
	var finishCondition = function(){
		return !$(prefix + ".task-" + activeTask + " .ball").length;
	};
	
	var finishFunction = function(){
		$(prefix + ".task-" + activeTask + " .check-button").fadeIn(0);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	
	var	checkButtonsListener = function(){
		var baskets = $(prefix + ".task-" + activeTask + " span");
		
		for(var i = 0; i < baskets.length; i++)
			$(baskets[i]).css("color", $(baskets[i]).attr("data-color"));
		
		timeout[1] = setTimeout(function(){
			for(var i = 0; i < baskets.length; i++)
			{
				var correctAnsw = $(baskets[i]).attr("data-correct");
				$(baskets[i]).html(correctAnsw);
				$(baskets[i]).css("width", correctAnsw.length + "em");
				$(baskets[i]).css("color", "green");
			}
		}, 3000);
		
		timeout[2] = setTimeout(function(){
			activeTask ++;
			tasks.fadeOut(0);
			if(activeTask < 3)
				$(prefix + ".task-" + activeTask).fadeIn(500);
			else
			{
				theFrame.attr("data-done", "true");
				sendCompletedStatement(1);
				fadeNavsInAuto();
			}
		}, 7000);
	};
	checkButtons.off("click", checkButtonsListener);
	checkButtons.on("click", checkButtonsListener);
	
	startButtonListener = function(){
		theFrame.attr("data-done", "true");
		$(prefix + ".task-" + activeTask).fadeIn(0);
		$(prefix + ".task-" + activeTask + " .check-button").fadeIn(0);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
};

launch["frame-114"] = function(){
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		darkTableHighlight = $(prefix + ".dark-table-highlight"),
		darkTableHighlight2 = $(prefix + ".dark-table-highlight-2");
		
	audio[0] = new Audio("audio/s24-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		darkTableHighlight2.fadeOut(0);
		theFrame.attr("data-done", "true");
		fadeNavsInAuto();
		sendCompletedStatement(13);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	darkTableHighlight.fadeOut(0);
	darkTableHighlight2.fadeOut(0);
		
	startButtonListener = function(){
		audio[0].play();
		timeout[1] = setTimeout(function(){
			darkTableHighlight.fadeIn(0);
		}, 1000);
		timeout[2] = setTimeout(function(){
			darkTableHighlight.fadeOut(0);
			darkTableHighlight2.fadeIn(0);
		}, 7000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-115"] = function(){
		theFrame = $("#frame-115"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		showingTable = $(prefix + ".showing-table"),
		presentingTablet = $(prefix + ".presenting-tablet"), 
		boyMouth = $(prefix + ".boy-mouth");
	
	sprite[0] = new Motio(presentingTablet[0], {
		fps: 3, 
		frames: 4
	});	
	
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	audio[0] = new Audio("audio/s25-1.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	presentingTablet.fadeOut(0);
	boyMouth.fadeOut(0);
		
	startButtonListener = function(){
		audio[0].play();
		boyMouth.fadeIn(0);
		
		timeout[1] = setTimeout(function()
		{
			showingTable.fadeOut(0);
			boyMouth.fadeOut(0);
			presentingTablet.fadeIn(0);
			sprite[0].play();
		}, 7000);
		
		timeout[2] = setTimeout(function()
		{
			audio[0].pause();
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(15);
		}, 8700);	
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(15);
	}, 2000);
}

launch["frame-116"] = function(){
		theFrame = $("#frame-116"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		givingFive = $(prefix + ".giving-five"),
		manMouth = $(prefix + ".man-mouth"), 
		shakingHands = $(prefix + ".shaking-hands");
	
	audio[0] = new Audio("audio/s25-1.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	manMouth.fadeOut(0);
	givingFive.fadeOut(0);
		
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			shakingHands.fadeOut(0);
			givingFive.fadeIn(0);
			audio[0].currentTime = 8.5;
			audio[0].play();
			manMouth.fadeIn(0);
		}, 2000);
		timeout[2] = setTimeout(function()
		{
			manMouth.fadeOut(0);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(8);
		}, 9000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(16);
	}, 2000);
}

launch["frame-301"] = function(){
		theFrame = $("#frame-301"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		allTasks = $(prefix + ".task"), 
		answers = $(prefix + ".answer"),
		checkButtons = $(prefix + ".check-button"), 
		touchElement = $(prefix + ".touch-element"), 
		activeTask = 1,
		result = $(prefix + ".task-result"), 
		pointsLabel = $(prefix + ".points-label"),
		points = 0,
		taskPrefix = prefix + ".task-" + activeTask;
		flips = $(prefix + ".flip"), 
		textfields = $(prefix + ".textfield"),
		firework = $(prefix + ".firework"),
		flipSprites = [],
		jeopardyLabel = $(prefix + ".jeopardy-label"),
		jeopardySprite = new Motio(jeopardyLabel[0], {
			fps: 2,
			frames: 5
		});
		jeopardySprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
		});
		
	audio[0] = new Audio("audio/jeopardy-theme.mp3");
	audio[1] = new Audio("audio/jeopardy.mp3");
	audio[2] = new Audio("audio/firework.mp3");
	
	var startQuestion = function()
	{
		if(activeTask <= 8)
			timeout[3] = setTimeout(function(){
				touchElement.fadeIn(0);
				allTasks.fadeOut(0);
				taskPrefix = prefix + ".task-" + activeTask;
				var elementButton = $(taskPrefix + ".element-button");
				$(taskPrefix + ".wink").fadeIn(0);
				elementButton.fadeIn(0);
				elementButton.click(function(){
					touchElement.fadeOut(0);
					$(taskPrefix + ".wink").fadeOut(0);
					$(flips[activeTask - 1]).fadeIn(0);
					flipSprites[activeTask - 1].play();
					$(taskPrefix + ".question").fadeIn(0);
					$(taskPrefix + ".answer").fadeIn(0);
					$(taskPrefix + ".textfield").fadeIn(0);
					$(taskPrefix + ".ball").fadeIn(0);
					$(taskPrefix + ".basket").fadeIn(0);
					if($(taskPrefix + ".ball").length)
						createDragTask(taskPrefix, $(taskPrefix + ".ball"), function(){
							points += 10;
							pointsLabel.html(points);
							pointsLabel.addClass("box-shadow-white");
							timeout[4] = setTimeout(function(){
								pointsLabel.removeClass("box-shadow-white");
							}, 2000);
							activeTask ++;
							startQuestion();
						});
					$(taskPrefix + ".check-button").fadeIn(0);
					$(taskPrefix + ".check-basket-button").fadeIn(0);
				});
			}, 2000);
		else
		{
			timeout[5] = setTimeout(function(){
				allTasks.fadeOut(0);
				result.fadeIn(0);
				result.html("Total points: " + points);
				firework.fadeIn(500);
				audio[2].play();
				timeout[6] = setTimeout(function(){
					firework.fadeOut(0);
					audio[2].pause();
				}, 6000);
			}, 2000);
		}
	}

	var answersListener = function(){
		var currAnswer = $(this);
		if(currAnswer.attr("data-correct"))
		{
			points += 10;
			pointsLabel.html(points);
			pointsLabel.addClass("box-shadow-white");
			timeout[7] = setTimeout(function(){
				pointsLabel.removeClass("box-shadow-white");
			}, 2000);
			
			currAnswer.css("color", "green");
		}
		else
		{
			currAnswer.css("color", "red");
		}
		activeTask ++;
		startQuestion();
	};
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	var checkButtonListener = function(){
		taskPrefix = prefix + ".task-" + activeTask;
		textfield = $(taskPrefix + ".textfield");
		if(textfield.val() !== "" && textfield.attr("data-correct").indexOf(textfield.val()) > -1)
		{
			points += 10;
			pointsLabel.html(points);
			pointsLabel.addClass("box-shadow-white");
			timeout[8] = setTimeout(function(){
				pointsLabel.removeClass("box-shadow-white");
			}, 2000);
			
			textfield.css("background-color", "green");
		}
		else
		{
			textfield.css("background-color", "red");
		}
		activeTask ++;
		startQuestion();
	}
	checkButtons
	checkButtons.off("click", checkButtonListener);
	checkButtons.on("click", checkButtonListener);
	textfields.val("");

	for(var i = 0; i < flips.length; i ++)
	{
		flipSprites[i] = new Motio(flips[i], {
			fps: 3,
			frames: 4
		});
		flipSprites[i].on("frame", function(){
			if(this.frame === this.frames - 1)
				this.pause();
		})
	}
	
	jeopardyLabel.fadeOut(0);
	allTasks.fadeOut(0);
	touchElement.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	firework.fadeOut(0);
	
	startButtonListener = function()
	{
		jeopardyLabel.fadeIn(0);
		timeout[1] = setTimeout(function(){
			audio[1].play();
			audio[0].play();
			jeopardySprite.play();
			timeout[2] = setTimeout(function(){
				startQuestion();
			}, 5000);
		}, 1000);
		timeout[2] = setTimeout(function(){
			audio[1].pause();
		}, 2500);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(301);
	}, 2000);
}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
	
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	elemId = elem.attr("id");
	regBox = $(".reg-box");
	fadeTimerOut();
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elemId === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();		

	for (var i = 0; i < timeout.length; i++)
	{
		clearTimeout(timeout[i]);
	}
	
	for (var i = 0; i < sprite.length; i++)
		sprite[i].pause();
	
	for (var i = 0; i < typingText.length; i++)
		typingText[i].stopWriting();
	
	if(elem.attr("id") === "frame-000")
		regBox.fadeIn(0);
	
	launch[elemId]();
	initMenuButtons();
}

var initMenuButtons = function(){
	links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
	
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	video[0].play(); 
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
    }, 15000);
};

$(document).ready(main);